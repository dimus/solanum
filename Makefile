PROJECT=solanum

VERSION = $(shell git describe --tags)
VER = $(shell git describe --tags --abbrev=0)
DATE = $(shell date -u '+%Y-%m-%d_%H:%M:%S%Z')

FLAGS_SHARED = CGO_ENABLED=1 GOARCH=amd64
FLAGS_LINUX = $(FLAGS_SHARED) GOOS=linux
FLAGS_MAC = $(FLAGS_SHARED) GOOS=darwin
FLAGS_WIN = $(FLAGS_SHARED) GOOS=windows
FLAGS_LD = -trimpath -ldflags "-w -s \
					 -X gitlab.com/dimus/$(PROJECT)/pkg.Build=$(DATE) \
           -X gitlab.com/dimus/$(PROJECT)/pkg.Version=$(VERSION)"
FLAGS_REL = -trimpath -ldflags "-s -w \
						-X gitlab.com/dimus/$(PROJECT)/pkg.Build=$(DATE)"

GOCMD = go
GOBUILD = $(GOCMD) build $(FLAGS_LD)
GOINSTALL = $(GOCMD) install $(FLAGS_LD)
GORELEASE = $(GOCMD) build $(FLAGS_REL)
GOCLEAN = $(GOCMD) clean
GOGET = $(GOCMD) get
GOTEST = $(GOCMD) test ./...

all: install

test: deps install
	$(GOTEST)

tools: deps
	@echo Installing tools from tools.go
	@cat tools.go | grep _ | awk -F'"' '{print $$2}' | xargs -tI % go install %

test-build: deps build

deps:
	@echo Download go.mod dependencies
	$(GOCMD) mod download;

## Version
version: ## Display current version
	@echo $(VERSION)

build:
	$(GOCLEAN); \
	$(FLAGS_SHARED) $(GOBUILD)

install:
	$(GOCLEAN); \
	CGO_ENABLED=1 $(GOINSTALL)

release: version asset
	$(GOCLEAN); \
	$(FLAGS_LINUX) $(GORELEASE); \
	tar zcf /tmp/$(PROJECT)-$(VER)-linux.tar.gz $(PROJECT); \
	$(GOCLEAN); \
	# $(FLAGS_MAC) $(GOBUILD); \
	# tar zcf /tmp/$(PROJECT)-$(VER)-mac.tar.gz $(PROJECT); \
	# $(GOCLEAN); \
	# $(FLAGS_WIN) $(GOBUILD); \
	# zip -9 /tmp/$(PROJECT)-$(VER)-win-64.zip $(PROJECT).exe; \
	# $(GOCLEAN);
