PROJECT=solanum
GOCMD = go
GOBUILD = $(GOCMD) build
GOINSTALL = $(GOCMD) install
GOCLEAN = $(GOCMD) clean
GOGET = $(GOCMD) get
GOTEST = $(GOCMD) test ./...
FLAG_MODULE = GO111MODULE=on
FLAGS_SHARED = $(FLAG_MODULE) CGO_ENABLED=1 GOARCH=amd64
FLAGS_LINUX = $(FLAGS_SHARED) GOOS=linux
FLAGS_MAC = $(FLAGS_SHARED) GOOS=darwin
FLAGS_WIN = $(FLAGS_SHARED) GOOS=windows

VERSION = $(shell git describe --tags)
VER = $(shell git describe --tags --abbrev=0)
DATE = $(shell date -u '+%Y-%m-%d_%H:%M:%S%Z')

all: install

test: deps install
	$(FLAG_MODULE) $(GOTEST)

test-build: deps build

deps:
	$(FLAG_MODULE) $(GOGET) github.com/shurcooL/vfsgen@6a9ea43; \
	$(FLAG_MODULE) $(GOGET) github.com/spf13/cobra/cobra@7547e83; \
	$(FLAG_MODULE) $(GOGET) github.com/onsi/ginkgo/ginkgo@505cc35; \
	$(FLAG_MODULE) $(GOGET) github.com/onsi/gomega@ce690c5; \
  $(FLAG_MODULE) $(GOGET) golang.org/x/tools/cmd/goimports

version:
	echo "package $(PROJECT)" > version.go
	echo "" >> version.go
	echo "const Version = \"$(VERSION)"\" >> version.go
	echo "const Build = \"$(DATE)\"" >> version.go

asset:
	cd fs; \
	$(FLAGS_SHARED) go run -tags=dev assets_gen.go

build: version asset
	cd $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_SHARED) $(GOBUILD)

install: version asset  ~/.config/solanum
	cd $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_SHARED) $(GOINSTALL)

release: version asset
	cd $(PROJECT); \
	$(GOCLEAN); \
	$(FLAGS_LINUX) $(GOBUILD); \
	tar zcf /tmp/$(PROJECT)-$(VER)-linux.tar.gz $(PROJECT); \
	$(GOCLEAN); \
	# $(FLAGS_MAC) $(GOBUILD); \
	# tar zcf /tmp/$(PROJECT)-$(VER)-mac.tar.gz $(PROJECT); \
	# $(GOCLEAN); \
	# $(FLAGS_WIN) $(GOBUILD); \
	# zip -9 /tmp/$(PROJECT)-$(VER)-win-64.zip $(PROJECT).exe; \
	# $(GOCLEAN);

~/.config/solanum:
	mkdir -p $@; \
	cp files/config.yml $@