//go:build tools
// +build tools

package main

import (
	// cobra is used for creating a scaffold of CLI applications
	_ "github.com/spf13/cobra"
	_ "github.com/spf13/cobra-cli"
)
