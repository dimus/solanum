package alert

import (
	"io"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"gitlab.com/dimus/solanum/fs"
)

var prepared = false
var s beep.StreamSeekCloser
var format beep.Format

func Play(path string) error {
	var f io.ReadCloser
	var err error
	if path == "" {
		f, err = fs.Files.Open("notification.wav")
		if err != nil {
			return err
		}
	} else {
		f, err = os.Open(path)
		if err != nil {
			return err
		}
	}
	s, format, err = wav.Decode(f)
	if err != nil {
		return err
	}

	if !prepared {
		err = speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
		if err != nil {
			return err
		}
		prepared = true
	}

	speaker.Play(s)

	return nil
}
