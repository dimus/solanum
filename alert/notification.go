package alert

import "github.com/0xAX/notificator"

var notify *notificator.Notificator

func Notify(title string, text string) {

	notify = notificator.New(notificator.Options{
		AppName: "solanum",
	})

	notify.Push(title, text, "", "")
}
