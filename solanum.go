package solanum

import (
	"fmt"
	"log"
	"os"
)

type timerType int

const (
	work timerType = iota
	shortBreak
	longBreak
)

type Solanum struct {
	Work           int
	ShortBreak     int
	LongBreak      int
	Tomatos        int
	Notification   bool
	Sound          bool
	WorkSoundPath  string
	BreakSoundPath string
}

type Option func(s *Solanum)

func OptWork(i int) Option {
	return func(s *Solanum) {
		s.Work = i
	}
}

func OptShortBreak(i int) Option {
	return func(s *Solanum) {
		s.ShortBreak = i
	}
}

func OptLongBreak(i int) Option {
	return func(s *Solanum) {
		s.LongBreak = i
	}
}

func OptNoNotification() Option {
	return func(s *Solanum) {
		s.Notification = false
	}
}

func OptNoSoundAlert() Option {
	return func(s *Solanum) {
		s.Sound = false
	}
}

func OptWorkSoundFile(path string) Option {
	return func(s *Solanum) {
		checkPath(path)
		s.WorkSoundPath = path
	}
}

func OptBreakSoundFile(path string) Option {
	return func(s *Solanum) {
		checkPath(path)
		s.BreakSoundPath = path
	}
}

func NewSolanum(opts ...Option) *Solanum {
	s := &Solanum{
		Work:         25,
		ShortBreak:   5,
		LongBreak:    15,
		Notification: true,
		Sound:        true,
	}
	for _, opt := range opts {
		opt(s)
	}
	return s
}

func fileExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func checkPath(path string) {
	var err error
	if !fileExists(path) {
		err = fmt.Errorf("File '%s' does not exist.", path)
	}
	if path[len(path)-4:] != ".wav" {
		err = fmt.Errorf("File '%s' is not a wav file.", path)
	}
	if err != nil {
		log.Fatal(err)
	}
}
