module gitlab.com/dimus/solanum

require (
	github.com/0xAX/notificator v0.0.0-20181105090803-d81462e38c21
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/faiface/beep v0.0.0-20181220115448-082eb9168dd1
	github.com/hajimehoshi/oto v0.3.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcool/vfsgen v0.0.0-20200627165143-92b8a710ab6c // indirect
	github.com/spf13/cobra v0.0.4-0.20190109003409-7547e83b2d85
	github.com/spf13/viper v1.3.1
)

go 1.14
