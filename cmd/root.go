// Copyright © 2019 Dmitry Mozzherin <dmozzherin@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	_ "embed"
	"fmt"
	"log/slog"
	"os"
	"path/filepath"

	"github.com/gnames/gnsys"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	solanum "gitlab.com/dimus/solanum/pkg"
)

//go:embed solanum.yaml
var configYAML string

//go:embed notification.wav
var sound []byte

var cfgFile string
var verbose bool
var pathSound = "notification.wav"

type config struct {
	WorkDuration        int
	ShortBreakDuration  int
	LongBreakDuration   int
	NoNotification      bool
	NoSoundAlert        bool
	WorkSoundAlertPath  string
	BreakSoundAlertPath string
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "solanum",
	Short: "Pomodoro timer for terminal",
	Long: `This program implements pomodoro timer for terminal window.

Solanum is a name of a genus for tomato, potato, eggplant and
many other plants.`,

	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		versionFlag(cmd)
		opts := getOpts()
		s := solanum.NewSolanum(opts...)
		s.Run()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().BoolP("version", "V", false,
		"shows build version and date, ignores other flags.")
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	var confDir string
	// Find home directory.
	home, err := os.UserHomeDir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	confFile := "config"
	confDir = filepath.Join(home, ".config", "solanum")
	pathSound = filepath.Join(confDir, pathSound)

	// Search config in home directory with name ".solanum" (without extension).
	viper.AddConfigPath(confDir)
	viper.SetConfigName(confFile)

	path := filepath.Join(confDir, confFile+".yaml")
	err = touchConfigFile(path)
	if err != nil {
		slog.Error(err.Error())
	}

	// If a config file is found, read it in.
	err = viper.ReadInConfig()
	if err != nil {
		slog.Warn(
			fmt.Sprintf("Cannot use config file '%s'", viper.ConfigFileUsed()),
		)
	}
}

func versionFlag(cmd *cobra.Command) {
	version, err := cmd.Flags().GetBool("version")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if version {
		fmt.Printf("\nversion: %s\n\nbuild:   %s\n\n",
			solanum.Version, solanum.Build)
		os.Exit(0)
	}
}

func getOpts() []solanum.Option {
	var opts []solanum.Option
	cfg := &config{}
	err := viper.Unmarshal(cfg)
	if err != nil {
		slog.Error(err.Error())
		os.Exit(1)
	}

	if cfg.WorkDuration != 0 {
		opts = append(opts, solanum.OptWork(cfg.WorkDuration))
	}
	if cfg.ShortBreakDuration != 0 {
		opts = append(opts, solanum.OptShortBreak(cfg.ShortBreakDuration))
	}
	if cfg.LongBreakDuration != 0 {
		opts = append(opts, solanum.OptLongBreak(cfg.LongBreakDuration))
	}
	if cfg.NoNotification {
		opts = append(opts, solanum.OptNoNotification())
	}
	if cfg.NoSoundAlert {
		opts = append(opts, solanum.OptNoSoundAlert())
	}
	if cfg.BreakSoundAlertPath != "" {
		opts = append(opts, solanum.OptBreakSoundFile(cfg.BreakSoundAlertPath))
	} else {
		opts = append(opts, solanum.OptBreakSoundFile(pathSound))
	}
	if cfg.WorkSoundAlertPath != "" {
		opts = append(opts, solanum.OptWorkSoundFile(cfg.WorkSoundAlertPath))
	} else {
		opts = append(opts, solanum.OptWorkSoundFile(pathSound))
	}
	return opts
}

// touchConfigFile checks if config file exists, and if not, it gets created.
func touchConfigFile(configPath string) error {
	exists, _ := gnsys.FileExists(configPath)
	if exists {
		return nil
	}

	slog.Info("Creating config file", "path", configPath)
	return createConfig(configPath)
}

// createConfig creates config file.
func createConfig(path string) error {
	err := gnsys.MakeDir(filepath.Dir(path))
	if err != nil {
		slog.Warn(fmt.Sprintf("Cannot create dir %s.", path))
		return err
	}

	err = os.WriteFile(path, []byte(configYAML), 0644)
	if err != nil {
		slog.Warn(fmt.Sprintf("Cannot write to file %s", path))
		return err
	}
	err = os.WriteFile(pathSound, sound, 0644)
	if err != nil {
		slog.Warn(fmt.Sprintf("Cannot write to file %s", path))
		return err
	}
	return nil
}
