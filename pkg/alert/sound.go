package alert

import (
	"embed"
	"log/slog"
	"os"
	"os/exec"
)

var f embed.FS

var prepared = false

func Play(path string) {
	cmd := exec.Command("paplay", path)

	err := cmd.Run()
	if err != nil {
		slog.Error("paplay cannot play the sound")
		slog.Error(err.Error())
		os.Exit(1)
	}
}
