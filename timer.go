package solanum

import (
	"bufio"
	"fmt"
	"os"
	"time"

	"gitlab.com/dimus/solanum/alert"
)

type Event struct {
	Type string
	Min  int
	Sec  int
}

func (s *Solanum) Run() {
	fmt.Print("\x1b[H\033[2J")
	tick := make(chan int)
	event := &Event{
		Type: "wrk",
		Min:  s.Work,
		Sec:  0,
	}
	go timeTick(tick)
	for range tick {
		s.runTimer(event)
		s.Display(event)
	}
}

func timeTick(tick chan<- int) {
	for {
		tick <- 1
		time.Sleep(1 * time.Second)
	}
}

func (s *Solanum) runTimer(e *Event) {
	if e.Sec == 0 {
		e.Min--
		e.Sec = 59
		return
	}
	e.Sec--
}

func (s *Solanum) Display(e *Event) {
	fmt.Print("\x1b[2K\r")

	if e.Min < 0 {
		s.switchEvent(e)
		bufio.NewReader(os.Stdin).ReadBytes('\n')
		fmt.Print("\x1b[H\033[2J")
	}
	fmt.Scan(os.Stdin)
	fmt.Printf("\x1b[1;33m%02d:%02d\x1b[0m %s %dt",
		e.Min, e.Sec, e.Type, s.Tomatos)
}

func (s *Solanum) switchEvent(e *Event) {
	prompt := "\x1b[1;5;33m00:00\x1b[0m %s"
	if e.Type == "wrk" {
		if s.Notification {
			go alert.Notify("Work is done!", "Take a break...")
		}
		if s.Sound {
			go alert.Play(s.BreakSoundPath)
		}
		e.Type = "brk"
		e.Min = s.ShortBreak - 1
		s.Tomatos++
		if s.Tomatos%4 == 0 {
			e.Min = s.LongBreak - 1
		}
		fmt.Printf(prompt, "brk?")
		return
	}
	if s.Notification {
		go alert.Notify("Break is done!", "Start working...")
		go alert.Play(s.WorkSoundPath)
	}
	e.Type = "wrk"
	e.Min = s.Work - 1
	fmt.Printf(prompt, "wrk?")
}
