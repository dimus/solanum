// Copyright © 2019 Dmitry Mozzherin <dmozzherin@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/dimus/solanum"
)

var cfgFile string
var verbose bool

type config struct {
	WorkDuration        int
	ShortBreakDuration  int
	LongBreakDuration   int
	NoNotification      bool
	NoSoundAlert        bool
	WorkSoundAlertPath  string
	BreakSoundAlertPath string
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "solanum",
	Short: "Pomodoro timer for terminal",
	Long: `This program implements pomodoro timer for terminal window.

Solanum is a name of a genus for tomato, potato, eggplant and
many other plants.`,

	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		versionFlag(cmd)
		opts := getOpts()
		s := solanum.NewSolanum(opts...)
		s.Run()
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().BoolP("version", "V", false,
		"shows build version and date, ignores other flags.")
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.solanum.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	// rootCmd.Flags().BoolP("verbose", "v", false, "more output")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		confDir := filepath.Join(home, ".config", "solanum")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".solanum" (without extension).
		viper.AddConfigPath(confDir)
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	// if err := viper.ReadInConfig(); err == nil {
	// 	fmt.Println("Using config file:", viper.ConfigFileUsed())
	// }
}

func versionFlag(cmd *cobra.Command) {
	version, err := cmd.Flags().GetBool("version")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	if version {
		fmt.Printf("\nversion: %s\n\nbuild:   %s\n\n",
			solanum.Version, solanum.Build)
		os.Exit(0)
	}
}

func getOpts() []solanum.Option {
	var opts []solanum.Option
	cfg := &config{}
	err := viper.Unmarshal(cfg)
	if err != nil {
		log.Fatal(err)
	}

	if cfg.WorkDuration != 0 {
		opts = append(opts, solanum.OptWork(cfg.WorkDuration))
	}
	if cfg.ShortBreakDuration != 0 {
		opts = append(opts, solanum.OptShortBreak(cfg.ShortBreakDuration))
	}
	if cfg.LongBreakDuration != 0 {
		opts = append(opts, solanum.OptLongBreak(cfg.LongBreakDuration))
	}
	if cfg.NoNotification {
		opts = append(opts, solanum.OptNoNotification())
	}
	if cfg.NoSoundAlert {
		opts = append(opts, solanum.OptNoSoundAlert())
	}
	if cfg.BreakSoundAlertPath != "" {
		opts = append(opts, solanum.OptBreakSoundFile(cfg.BreakSoundAlertPath))
	}
	if cfg.WorkSoundAlertPath != "" {
		opts = append(opts, solanum.OptWorkSoundFile(cfg.WorkSoundAlertPath))
	}
	return opts
}
